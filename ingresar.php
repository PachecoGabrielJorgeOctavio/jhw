<?php

session_start();
require_once "./config.php";

/* @var $_POST type */
$correo = $_POST["txtcorreo"];
$pass = $_POST["txtpassword"];
$correo = mysqli_real_escape_string($mysqli, $correo);
$pass = mysqli_real_escape_string($mysqli, $pass);

/*La busqueda en la base de datos se realiza de este modo para evitar las inyecciones sql*/
$query = ("SELECT Nombre,Correo,Contraseña FROM personas WHERE Correo = ?");
$stmt = $mysqli->prepare($query);
$stmt->bind_param("s", $param_correo);
$param_correo = $correo;
if ($stmt->execute()) {
  $stmt->store_result();
  if ($stmt->num_rows == 1) {
    $stmt->bind_result($nombre, $correo, $hashed_password);
    if ($stmt->fetch()) {
      if (password_verify($pass, $hashed_password)) {
        $_SESSION['correo'] = $correo;
        $_SESSION['usuario'] = $nombre;
        if ($correo == "juliogabriel@gmail.com") {
          header("Location: vistaAdmin.php");
        } else {
          header("Location: vistaCliente.php");
        }
      } else {
        header("Location: login.html");
      }
    } else {
      header("Location: login.html");
    }
  } else {
    header("Location: login.html");
  }
} else {
}
