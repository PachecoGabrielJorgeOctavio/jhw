<?php
function validateEmail($email) {
    if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "{$email}: A valid email"."<br>";
    }
    else {
        echo "{$email}: Not a valid email"."<br>";
    }
}
validateEmail('peter.piper@iana.org');
validateEmail('first.last@example.123');
?>