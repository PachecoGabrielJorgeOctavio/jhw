<?php
require_once "config.php";

$correo = $password = $confirm_password = $nombre = $telefono = $direc = "";
$correo_err = $password_err = $confirm_password_err = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // Validando correo
  if (empty(trim($_POST["correo"]))) {
    $correo_err = "Por favor Ingrese un Correo Electronico.";
  } elseif (!filter_var($_POST["correo"],FILTER_VALIDATE_EMAIL)) {
    $correo_err = "Correo no valido";
  } else {
    // Prepare a select statement
    $sql = "SELECT Correo FROM personas WHERE Correo = ?";
    if ($stmt = $mysqli->prepare($sql)) {
      // Bind variables to the prepared statement as parameters
      $stmt->bind_param("s", $param_correo);

      // Set parameters
      $param_correo = trim($_POST["correo"]);

      // Attempt to execute the prepared statement
      if ($stmt->execute()) {
        // store result
        $stmt->store_result();

        if ($stmt->num_rows == 1) {
          $correo_err = "Este correo ya se encuentra registrado.";
        } else {
          $correo = trim($_POST["correo"]);
        }
      } else {
        echo "Oops! Something went wrong. Please try again later.";
      }

      // Close statement
      $stmt->close();
    }
  }

  // Validate password
  if (empty(trim($_POST["password"]))) {
    $password_err = "Por favor ingrese una contraseña.";
  } elseif (strlen(trim($_POST["password"])) < 6) {
    $password_err = "La contraseña debe tener mas de 6 carácteres.";
  } else {
    $password = trim($_POST["password"]);
  }

  // Validate confirm password
  if (empty(trim($_POST["confirm_password"]))) {
    $confirm_password_err = "Por favor confirma la contraseña.";
  } else {
    $confirm_password = trim($_POST["confirm_password"]);
    if (empty($password_err) && ($password != $confirm_password)) {
      $confirm_password_err = "Las contraseñas no coinciden";
    }
  }

  // Check input errors before inserting in database
  if (empty($correo_err) && empty($password_err) && empty($confirm_password_err)) {

    // Prepare an insert statement
    $sql = "INSERT INTO personas (Correo, Contraseña,Nombre,Telefono,direccion) VALUES (?, ?,?,?,?)";

    if ($stmt = $mysqli->prepare($sql)) {
      // Bind variables to the prepared statement as parameters
      $stmt->bind_param("sssss", $param_correo, $param_password,$param_nom,$param_tel,$param_dir);

      // Set parameters
      $param_corre = $correo;
      $param_password = password_hash($password, PASSWORD_DEFAULT); 
      $param_nom = trim($_POST["nombre"]);
      $param_tel = trim($_POST["telefono"]);
      $param_dir = trim($_POST["direccion"]);

      // Attempt to execute the prepared statement
      if ($stmt->execute()) {
        // Redirect to login page
        header("location: login.html");
      } else {
        echo "Oops! Something went wrong. Please try again later.";
      }

      // Close statement
      $stmt->close();
    }
  }

  // Close connection
  $mysqli->close();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Registrarse</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    body {
      font: 14px sans-serif;
    }

    .wrapper {
      width: 360px;
      padding: 20px;
    }
  </style>
</head>

<body>
  <div class="wrapper">
    <h2>Registro</h2>
    <p>Ingresa tus datos correctamente</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
      <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="nombre" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Telefono</label>
        <input type="text" name="telefono" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Direccion</label>
        <input type="text" name="direccion" class="form-control" required>
      </div>
      <div class="form-group">
        <label>Correo</label>
        <input type="email" name="correo" class="form-control <?php echo (!empty($correo_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
        <span class="invalid-feedback"><?php echo $correo_err; ?></span>
      </div>
      <div class="form-group">
        <label>Contraseña</label>
        <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
        <span class="invalid-feedback"><?php echo $password_err; ?></span>
      </div>
      <div class="form-group">
        <label>Confirmar Contraseña</label>
        <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" >
        <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Submit">
        <input type="reset" class="btn btn-secondary ml-2" value="Rehacer">
      </div>
      <p>Ya tienes una cuenta? <a href="login.html">Ingresa aqui</a>.</p>
    </form>
  </div>
</body>

</html>